import Vue from 'vue'

import App from './App'
import router from './router'
import store from './store'
import plugins from './plugins'
import filters from './filters'

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.config.productionTip = false

plugins(Vue)
filters(Vue)

/* eslint-disable no-new */
new Vue({
  components: { App },
  router,
  store,
  template: '<App/>'
}).$mount('#app')
