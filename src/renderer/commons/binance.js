import WebSocket from 'ws'
import axios     from 'axios'

const WS_URL = 'wss://stream.binance.com:9443/ws/'
const API_URL = 'https://api.binance.com/api/v1/'

const responseDataKeys = {
  e: 'eventType',
  E: 'time',
  s: 'coin',
  P: 'priceChange',
  p: 'percentChange',
  w: 'weightedAverage',
  x: 'previousClose',
  c: 'close',
  Q: 'quantity',
  b: 'bid',
  B: 'bidQuantity',
  a: 'ask',
  A: 'askQuantity',
  o: 'open',
  h: 'high',
  l: 'low',
  v: 'totalTradedBaseAssetVolume',
  q: 'totalTradedQuoteAssetVolume',
  O: 'openTime',
  C: 'closeTime',
  n: 'tradesCount'
}


class Binance {
  parseWsData (data) {
    const { E, s, p, P, c, o, h, l, v } = data
    return {
      timestamp: E,
      quote: s.substring(3, 6),
      coin: s.substring(0, 3),
      change: parseFloat(p),
      percentChange: parseFloat(P),
      open: parseFloat(o),
      close: parseFloat(c),
      high: parseFloat(h),
      low: parseFloat(l),
      volume: parseInt(v)
    }
  }

  bindTickers (symbol, fn) {
    let streamName = ''
    if (typeof symbol === 'function') {
      fn = symbol
      streamName = '!ticker@arr'
    } else {
      streamName = symbol.toLowerCase() + 'btc@ticker'
    }

    const tickerWs = new WebSocket(WS_URL + streamName)
    tickerWs.on('open', () => {
      console.log('Connected to ', streamName)
      tickerWs.on('message', data => {
        const parsedData = JSON.parse(data)
        const result = parsedData instanceof Array ? parsedData.map(datum => this.parseWsData(datum)) : this.parseWsData(parsedData)
        fn(result)
      })
    })
    return tickerWs
  }

  /**
   * Returns prices with earliest at index 0
   * @param from
   * @param interval
   * @returns {Promise<*>}
   */
  async fetchPrices (from, interval = 'day') {
    if (!from || !interval) throw new Error('From symbol is required')

    interval = 1 + interval.charAt(0)

    const toSymbol = 'BTC'

    const data = await axios.get(API_URL + 'klines?symbol=' + from.toUpperCase() + toSymbol + '&interval=' + interval)
    return this.parseRestData(data)
  }

  parseRestData (data) {
    return data.map(([timestamp, open, high, low, close, volume, quote, tradeCount]) => {
      return {
        timestamp,
        open: parseFloat(open),
        high: parseFloat(high),
        low: parseFloat(low),
        close: parseFloat(close),
        volume: parseFloat(volume),
        quote,
        tradeCount: parseFloat(tradeCount)
      }
    })
  }
}

export default new Binance()
