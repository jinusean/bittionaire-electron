const components = {
  Info: require('./Info').default,
  ALink: require('./ALink').default
}


export default function (Vue) {
  Object.keys(components).forEach(key => {
    Vue.component(key, components[key])
  })
}
