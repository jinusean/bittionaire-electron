import axios from 'axios'

axios.interceptors.response.use((response) => {
  return response.data;
}, function (error) {
  const data = error.response ? error.response.data : error.response
  return Promise.reject(data);
});

export default axios
