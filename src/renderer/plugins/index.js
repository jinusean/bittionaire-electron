import components from './components'
import vtooltip   from 'v-tooltip'

const vuePackages = [
  vtooltip
]

const nonvuePackages = {
  axios: require('./axios'),
  ws: require('ws'),
  moment: require('moment'),
  binance: require('@/commons/binance').default,
  indicators: require('technicalindicators')
}


export default function (Vue) {
  components(Vue)

  vuePackages.forEach(plugin => {
    Vue.use(plugin)
  })

  Object.keys(nonvuePackages).forEach(plugin => {
    const pckg = nonvuePackages[plugin]
    Vue[plugin] = Vue.prototype['$' + plugin] = pckg
  })

}
