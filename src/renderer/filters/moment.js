import moment from 'moment'

export default function (value, format) {
  let datetime = value ? moment(value) : moment()
  return datetime.format(format)
}