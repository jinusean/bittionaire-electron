import fs from 'fs'

export default function (Vue) {
  fs.readdirSync(__dirname).forEach(filename => {
    if (filename === 'index.js') {
      return
    }
    const name = filename.split('.js')[0]
    const pckg = require('./' + filename).default

    Vue.filter(name, pckg)
  })
}
