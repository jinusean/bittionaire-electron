import numeral from 'numeral'

export default function (value, format, option) {
  value = value || 0


  switch (format) {
    case 'fixed':
      format = '0,0.'
      while (option  && option > 0) {
        format += '0'
        option--
      }
      break
    case 'currency':
      format = '$0,0'
      if (!option) {
        format += '.00'
      }
      break
  }

  return numeral(value).format(format)
}